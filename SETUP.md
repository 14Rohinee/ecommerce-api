This is project setup guide

husky pre-commit file requires to be executable. By runnung chmod +x .husky/pre-commit you can make it executable.

In case if husky pre-commit file is not executable, reinitialize husky by running
npx husky-init && npm install

Package used -
migrate Mongo - https://github.com/riyadhuddin/membership_mongo
migrate-mongo create <FILENAME>
migrate-mongo up
migrate-mongo down
migrate-mongo status

Note - There is some issues with migration package. So whenever it does not work, please run this command npm install -g migrate-mongo and to test if it is working or not, run migrate-mongo status