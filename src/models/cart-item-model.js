import mongoose from 'mongoose';
import cartItemSchema from '../schema/cart-item-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('CartItem', cartItemSchema); // (<CollectionName>, <CollectionSchema>)
