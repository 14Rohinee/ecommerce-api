import mongoose from 'mongoose';
import userSchema from '../schema/user-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('User', userSchema); // (<CollectionName>, <CollectionSchema>)
