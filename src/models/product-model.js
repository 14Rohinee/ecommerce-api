import mongoose from 'mongoose';
import productSchema from '../schema/product-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('Product', productSchema); // (<CollectionName>, <CollectionSchema>)
