import mongoose from 'mongoose';
import orderSchema from '../schema/order-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('Order', orderSchema); // (<CollectionName>, <CollectionSchema>)
