import mongoose from 'mongoose';
import reviewSchema from '../schema/review-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('Review', reviewSchema); // (<CollectionName>, <CollectionSchema>)
