import mongoose from 'mongoose';
import languageSchema from '../schema/language-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('Language', languageSchema); // (<CollectionName>, <CollectionSchema>)
