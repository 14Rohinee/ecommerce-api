import mongoose from 'mongoose';
import emailSettingSchema from '../schema/email-setting-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('EmailSetting', emailSettingSchema); // (<CollectionName>, <CollectionSchema>)
