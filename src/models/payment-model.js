import mongoose from 'mongoose';
import paymentSchema from '../schema/payment-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('Payment', paymentSchema); // (<CollectionName>, <CollectionSchema>)
