import mongoose from 'mongoose';
import brandSchema from '../schema/brand-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('Brand', brandSchema); // (<CollectionName>, <CollectionSchema>)
