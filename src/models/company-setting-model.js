import mongoose from 'mongoose';
import companySettingSchema from '../schema/company-setting-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('CompanySetting', companySettingSchema); // (<CollectionName>, <CollectionSchema>)
