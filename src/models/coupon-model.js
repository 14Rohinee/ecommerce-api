import mongoose from 'mongoose';
import couponSchema from '../schema/coupon-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('Coupon', couponSchema); // (<CollectionName>, <CollectionSchema>)
