import mongoose from 'mongoose';
import shippingAddressSchema from '../schema/shipping-address-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('shippingAddresses', shippingAddressSchema); // (<CollectionName>, <CollectionSchema>)
