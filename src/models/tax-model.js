import mongoose from 'mongoose';
import taxSchema from '../schema/tax-schema.js';

export default mongoose.model('Tax', taxSchema); // (<CollectionName>, <CollectionSchema>)
