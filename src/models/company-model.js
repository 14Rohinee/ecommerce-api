import mongoose from 'mongoose';
import companySchema from '../schema/company-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('Company', companySchema); // (<CollectionName>, <CollectionSchema>)
