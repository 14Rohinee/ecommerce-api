import express from 'express';
import 'dotenv/config';
import mongoose from 'mongoose';
import './cron-jobs/index.js';
import logger from './services/loggerService.js';
import routes from './routes/index.js';

const app = express();
app.use(routes);

// Connect to MongoDB and start the server
async function startServer () {
    const PORT = process.env.PORT || process.env.APP_PORT;
    const MONGO_URL = process.env.MONGO_URL;
    const MONGO_DBNAME = process.env.MONGO_DBNAME;
    const MONGO_QUERY_PARAMS = process.env.MONGO_QUERY_PARAMS;

    try {
        // Connect to MongoDB
        await mongoose.connect(`${MONGO_URL}${MONGO_DBNAME}${MONGO_QUERY_PARAMS}`);

        // Start Express server
        app.listen(PORT, () => {
            console.log(`Server is running on port ${PORT}`);
            logger.info(`Server is running on port ${PORT}`);
        });
    } catch (error) {
        console.error('Error connecting to MongoDB:', error);
        logger.error('Error connecting to MongoDB:', error);
        process.exit(1); // Exit the process with a non-zero status code
    }
}

startServer(); // Start the server

export default app;
