import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * Represents the schema for a cart in the e-commerce application.
 *
 * @typedef {Object} CartSchema
 * @property {Schema.Types.ObjectId} companyId - The ID of the company associated with the cart.
 * @property {Schema.Types.ObjectId} userId - The ID of the user who owns the cart.
 * @property {Schema.Types.ObjectId} couponId - The ID of the coupon applied to the cart (optional).
 * @property {Schema.Types.ObjectId} orderId - The ID of the order associated with the cart (optional).
 * @property {Schema.Types.ObjectId} addressId - The ID of the address associated with the cart.
 * @property {Number} subTotal - The subtotal of the cart.
 * @property {Number} discount - The discount applied to the cart (default: 0).
 * @property {Number} tax - The tax applied to the cart (default: 0).
 * @property {Number} deliveryFee - The delivery fee for the cart (default: 0).
 * @property {Number} total - The total amount of the cart.
 * @property {String} deliveryInstruction - The delivery instruction for the cart (optional).
 * @property {String} paymentMethod - The payment method for the cart (enum: ['Stripe', 'Paypal', 'Razorpay', 'COD'], default: 'COD').
 * @property {String} status - The status of the cart (enum: ['Ordered', 'Canceled', 'Delivered'], default: 'Ordered').
 * @property {Schema.Types.ObjectId} createdBy - The ID of the user who created the cart (optional).
 * @property {Date} createdAt - The date and time when the cart was created (default: current date and time).
 * @property {Schema.Types.ObjectId} updatedBy - The ID of the user who last updated the cart (optional).
 * @property {Date} updatedAt - The date and time when the cart was last updated (optional).
 * @property {Schema.Types.ObjectId} deletedBy - The ID of the user who soft deleted the cart (optional).
 * @property {Date} deletedAt - The date and time when the cart was soft deleted (optional).
 */
const cartSchema = new Schema({
    companyId: { type: Schema.Types.ObjectId, required: false },
    sessionId: { type: String, required: false },
    userId: { type: Schema.Types.ObjectId, required: false },
    couponId: { type: Schema.Types.ObjectId, required: false },
    orderId: { type: Schema.Types.ObjectId, required: false },
    shippingAddressId: { type: Schema.Types.ObjectId, required: false },
    subTotal: { type: Number, required: false },
    discount: { type: Number, default: 0 },
    tax: { type: Number, default: 0 },
    deliveryFee: { type: Number, default: 0 },
    total: { type: Number, required: true },
    deliveryInstruction: { type: String, required: false },
    paymentMethod: { type: String, enum: ['Stripe', 'Paypal', 'Razorpay', 'COD'], default: 'COD' },
    status: { type: String, enum: ['In Progress', 'Ordered', 'Canceled', 'Delivered'], default: 'In Progress' },
    createdBy: { type: Schema.Types.ObjectId, required: false },
    createdAt: { type: Date, default: Date.now },
    updatedBy: { type: Schema.Types.ObjectId, required: false },
    updatedAt: { type: Date, required: false },
    deletedBy: { type: Schema.Types.ObjectId, required: false }, // Soft delete
    deletedAt: { type: Date, required: false }
}, { collection: 'carts' });

cartSchema.plugin(uniqueValidator);

export default cartSchema;
