import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * Represents the subscription schema.
 * @typedef {Object} SubscribeSchema
 * @property {string} email - The email of the subscriber. (required)
 * @property {string} status - The status of the subscription. Can be either 'subscribed' or 'unsubscribed'. (default: 'subscribed')
 */
const subscribeSchema = new Schema({
    email: { type: String, required: true, unique: true },
    status: { type: String, enum: ['subscribed', 'unsubscribed'], default: 'subscribed' }
});

subscribeSchema.plugin(uniqueValidator);

export default subscribeSchema;
