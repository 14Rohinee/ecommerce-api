import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * Mongoose schema for SMS settings.
 *
 * @typedef {Object} smsSettingSchema
 * @property {Schema.Types.ObjectId} companyId - The ID of the company associated with the SMS settings.
 * @property {String} smsSid - The SID (account identifier) for the SMS service.
 * @property {String} smsToken - The authentication token for the SMS service.
 * @property {String} smsFromNumber - The phone number used as the sender for SMS messages.
 * @property {String} status - The status of the SMS settings. Can be 'active' or 'inactive'.
 * @property {Schema.Types.ObjectId} updatedBy - The ID of the user who last updated the SMS settings.
 * @property {Date} updatedAt - The date and time when the SMS settings were last updated.
 *
 * @memberof module:sms-setting-schema
 * @inner
 */
const smsSettingSchema = new Schema({
    companyId: { type: Schema.Types.ObjectId, required: false },
    smsSid: { type: String, required: true },
    smsAuthToken: { type: String, required: true },
    smsFromNumber: { type: String, required: true },
    status: { type: String, enum: ['active', 'inactive'], default: 'inactive' },
    updatedBy: { type: Schema.Types.ObjectId, required: false },
    updatedAt: { type: Date, required: false }
}, { collection: 'smsSettings' });

smsSettingSchema.plugin(uniqueValidator);

export default smsSettingSchema;
