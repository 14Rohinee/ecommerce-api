import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * Represents the schema for the company settings.
 *
 * @typedef {Object} CompanySettingSchema
 * @property {Schema.Types.ObjectId} companyId - The ID of the company.
 * @property {boolean} paypalStatus - The status of PayPal integration.
 * @property {string} paypalEnvironment - The environment for PayPal integration (sandbox or live).
 * @property {string} paypalSandboxClientId - The client ID for PayPal sandbox environment.
 * @property {string} paypalSandboxClientSecret - The client secret for PayPal sandbox environment.
 * @property {string} paypalLiveClientId - The client ID for PayPal live environment.
 * @property {string} paypalLiveClientSecret - The client secret for PayPal live environment.
 * @property {boolean} stripeStatus - The status of Stripe integration.
 * @property {string} stripeEnvironment - The environment for Stripe integration (sandbox or live).
 * @property {string} stripeSandboxPublishableKey - The publishable key for Stripe sandbox environment.
 * @property {string} stripeSandboxSecretKey - The secret key for Stripe sandbox environment.
 * @property {string} stripeLivePublishableKey - The publishable key for Stripe live environment.
 * @property {string} stripeLiveSecretKey - The secret key for Stripe live environment.
 * @property {boolean} razorpayStatus - The status of Razorpay integration.
 * @property {string} razorpayEnvironment - The environment for Razorpay integration (sandbox or live).
 * @property {string} razorpaySandboxKeyId - The key ID for Razorpay sandbox environment.
 * @property {string} razorpaySandboxKeySecret - The key secret for Razorpay sandbox environment.
 * @property {string} razorpayLiveKeyId - The key ID for Razorpay live environment.
 * @property {string} razorpayLiveKeySecret - The key secret for Razorpay live environment.
 * @property {Schema.Types.ObjectId} createdBy - The ID of the user who created the company settings.
 * @property {Date} createdAt - The date and time when the company settings were created.
 * @property {Schema.Types.ObjectId} updatedBy - The ID of the user who last updated the company settings.
 * @property {Date} updatedAt - The date and time when the company settings were last updated.
 */
const companySettingSchema = new Schema({
    companyId: { type: Schema.Types.ObjectId, required: true },
    paypalStatus: { type: Boolean, default: false },
    paypalEnvironment: { type: String, enum: ['sandbox', 'live'], default: 'sandbox' },
    paypalSandboxClientId: { type: String, required: false },
    paypalSandboxClientSecret: { type: String, required: false },
    paypalLiveClientId: { type: String, required: false },
    paypalLiveClientSecret: { type: String, required: false },
    stripeStatus: { type: Boolean, default: false },
    stripeEnvironment: { type: String, enum: ['sandbox', 'live'], default: 'sandbox' },
    stripeSandboxPublishableKey: { type: String, required: false },
    stripeSandboxSecretKey: { type: String, required: false },
    stripeLivePublishableKey: { type: String, required: false },
    stripeLiveSecretKey: { type: String, required: false },
    razorpayStatus: { type: Boolean, default: false },
    razorpayEnvironment: { type: String, enum: ['sandbox', 'live'], default: 'sandbox' },
    razorpaySandboxKeyId: { type: String, required: false },
    razorpaySandboxKeySecret: { type: String, required: false },
    razorpayLiveKeyId: { type: String, required: false },
    razorpayLiveKeySecret: { type: String, required: false },
    createdBy: { type: Schema.Types.ObjectId, required: false },
    createdAt: { type: Date, default: Date.now },
    updatedBy: { type: Schema.Types.ObjectId, required: false },
    updatedAt: { type: Date, required: false }
}, { collection: 'companySettings' });

companySettingSchema.plugin(uniqueValidator);

export default companySettingSchema;
