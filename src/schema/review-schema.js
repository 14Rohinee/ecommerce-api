import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * Represents the schema for a review in the e-commerce backend.
 *
 * @typedef {Object} ReviewSchema
 * @property {Schema.Types.ObjectId} userId - The ID of the user who created the review.
 * @property {Schema.Types.ObjectId} productId - The ID of the product being reviewed.
 * @property {number} rating - The rating given to the product (minimum value: 1).
 * @property {string} review - The text content of the review.
 * @property {string} status - The status of the review ('active' or 'inactive').
 * @property {Schema.Types.ObjectId} createdBy - The ID of the user who created the review.
 * @property {Date} createdAt - The date and time when the review was created.
 * @property {Schema.Types.ObjectId} updatedBy - The ID of the user who last updated the review.
 * @property {Date} updatedAt - The date and time when the review was last updated.
 * @property {Schema.Types.ObjectId} deletedBy - The ID of the user who deleted the review (soft delete).
 * @property {Date} deletedAt - The date and time when the review was deleted (soft delete).
 */
const reviewSchema = new Schema({
    userId: { type: Schema.Types.ObjectId, required: true },
    productId: { type: Schema.Types.ObjectId, required: true }, // Payment method
    rating: { type: Number, min: 1, default: 1 },
    review: { type: String, required: true },
    status: { type: String, enum: ['active', 'inactive'], default: 'active' },
    createdBy: { type: Schema.Types.ObjectId, required: false },
    createdAt: { type: Date, default: Date.now },
    updatedBy: { type: Schema.Types.ObjectId, required: false },
    updatedAt: { type: Date, required: false },
    deletedBy: { type: Schema.Types.ObjectId, required: false }, // Soft delete
    deletedAt: { type: Date, required: false }
});

reviewSchema.plugin(uniqueValidator);

export default reviewSchema;
