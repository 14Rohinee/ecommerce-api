import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * Represents the schema for a product in the e-commerce system.
 *
 * @typedef {Object} ProductSchema
 * @property {Schema.Types.ObjectId} companyId - The ID of the company associated with the product.
 * @property {Schema.Types.ObjectId} categoryId - The ID of the category associated with the product.
 * @property {Schema.Types.ObjectId} [subCategoryId] - The ID of the subcategory associated with the product (optional).
 * @property {Schema.Types.ObjectId} [brandId] - The ID of the brand associated with the product (optional).
 * @property {string} name - The name of the product.
 * @property {string} slug - The unique slug for the product.
 * @property {string} shortDescription - The short description of the product.
 * @property {string} longDescription - The long description of the product.
 * @property {string[]} [feature_images] - The URLs of the feature images for the product (optional).
 * @property {string[]} [images] - The URLs of the images for the product (optional).
 * @property {number} price - The price of the product.
 * @property {number} [discount] - The discount amount for the product (optional).
 * @property {string} [discountType] - The type of discount for the product (optional).
 * @property {number} totalQuantity - The total quantity of the product.
 * @property {number} [quantityInStock] - The quantity of the product currently in stock (optional).
 * @property {number} [displayOrder] - The display order of the product (optional).
 * @property {string} [status] - The status of the product (optional).
 * @property {Schema.Types.ObjectId} [createdBy] - The ID of the user who created the product (optional).
 * @property {Date} [createdAt] - The date and time when the product was created (optional).
 * @property {Schema.Types.ObjectId} [updatedBy] - The ID of the user who last updated the product (optional).
 * @property {Date} [updatedAt] - The date and time when the product was last updated (optional).
 * @property {Schema.Types.ObjectId} [deletedBy] - The ID of the user who deleted the product (optional).
 * @property {Date} [deletedAt] - The date and time when the product was deleted (optional).
 */
const productSchema = new Schema({
    companyId: { type: Schema.Types.ObjectId, required: true },
    categoryId: { type: Schema.Types.ObjectId, required: true },
    subCategoryId: { type: Schema.Types.ObjectId, required: true },
    brandId: { type: Schema.Types.ObjectId, required: true },
    name: { type: String, required: true },
    slug: { type: String, required: true, unique: true },
    shortDescription: { type: String, required: true },
    longDescription: { type: String, required: true },
    featureImage: [{ type: String, required: false }],
    images: [{ type: String, required: false }],
    price: { type: Number, required: true },
    discount: { type: Number, required: false },
    discountType: { type: String, enum: [null, 'percentage', 'fixed'], default: null },
    totalQuantity: { type: Number, required: false },
    quantityInStock: { type: Number, required: false },
    displayOrder: { type: Number, required: false },
    status: { type: String, enum: ['active', 'inactive'], default: 'inactive' },
    sku: { type: String, required: true },
    tags: [{ type: String, required: false }],
    createdBy: { type: Schema.Types.ObjectId, required: false },
    createdAt: { type: Date, default: Date.now },
    updatedBy: { type: Schema.Types.ObjectId, required: false },
    updatedAt: { type: Date, required: false },
    deletedBy: { type: Schema.Types.ObjectId, required: false }, // Soft delete
    deletedAt: { type: Date, required: false }
});

productSchema.plugin(uniqueValidator);

export default productSchema;
